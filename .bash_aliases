alias grep='/bin/grep --color=auto'
alias ls='ls --color=auto'
alias drush='/home/koen/Documents/fusenet/website/vendor/drush/drush/drush'
alias vsc='/mnt/c/Users/s157356/AppData/Local/Programs/Microsoft\ VS\ Code/Code.exe'
alias lampp='sudo /opt/lampp/lampp'
alias paraview="paraview.exe"
alias screen-session="xfce4-session"
alias ff++="/home/koen/FreeFem-sources/src/nw/FreeFem++"

# Folders
alias docs="cd ~/Documents"
alias winhome="cd /mnt/c/Users/s157356"
alias intern="cd ~/Documents/internship"


